GDPR for YouTube & Vimeo videos
-------------------------------
This module implements a requirement from the European General Data Protection
Regulation, which applies to websites targeting any of the European Union
member states. For details, see https://privacyinternational.org/long-read/2207/why-and-how-gdpr-applies-companies-globally.
If a website features embedded videos, it must not use cookies before the user
has given explicit consent.

This module automatically pops up a GDPR-style consent prompt over the top of
each YouTube or Vimeo remote video iframe on your site. These videos may be
embedded via a remote video field on any content type, or via Drupal's WYSIWYG
editor, through the the Media Embed button (requires the Media Library module
to also be enabled).

Rather than first loading the player and its controls, this module loads the
video's default frame as a plain image "teaser". This does not involve cookies
or other private data.
The module then places a consent prompt over the top of the image with an
"I agree" button. After the user has clicked the button, the video player is
loaded and its play button may be pressed to watch the video as per normal.

Prerequisites
-------------
Media Library module is enabled.
When using the WYSIWYG editor to insert videos (as opposed to using a field
on the content type), then at admin/config/content/formats/manage/full_html,
make sure the Media button is made available by dragging it into the toolbar
and enabling the Embed media filter for Remote Video.

Configuration
-------------
The GDPR consent prompt text and confirmation button label are configurable
at admin/config/regional/gdpr-video.
You may also enter translations for the above. To do this, first enable the
core module Configuration Translation.
Then visit admin/config/regional/gdpr-video/translate

Note regarding YouTube cookies
------------------------------
While the alternative YouTube domain youtube-nocookie.com may suggest that
no cookies are created on the user's machine at all, there certainly are, once
the user has pressed "play".
See https://axbom.blog/embed-youtube-videos-without-cookies

How can I see what cookies live on my machine?
----------------------------------------------
Most browsers have an advanced option for this.
For instance in Chrome, first select View -> Developer -> Developer Tools.
A panel opens at the bottom of the screen with a toolbar of many options.
Click Application to reveal a panel on the left.
Under the heading Storage, you'll see your Cookies and other types of local
storage appear.

In Firefox: Tools -> Web Developer -> Toggle Tools. Then click the Storage
tab.

