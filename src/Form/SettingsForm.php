<?php

namespace Drupal\gdpr_video\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure settings for this site.
 *
 * @internal
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a PerformanceForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gdpr_video_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gdpr_video.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('gdpr_video.settings');

    if ($this->moduleHandler->moduleExists('config_translation')) {
      $url_translate = Url::fromUri('internal:/admin/config/regional/gdpr-video/translate');
      $url = $url_translate ? $url_translate->toString() : '#';
      $description = $this->t('You may translate these texts by clicking the <a href=":url">Translate</a> tab.', [':url' => $url]);
    }
    else {
      // "Extend" tab in admin menu.
      $url_config = Url::fromUri('internal:/admin/modules#module-config-translation');
      $url = $url_config ? $url_config->toString() : '#';
      $description = $this->t('If you enable the core module <a href=":url">Configuration Translation</a>, you can translate these texts through the UI at <em>Administration &raquo; Configuration &raquo; Regional and language &raquo; GDPR video</em>.', [':url' => $url]);
    }
    global $base_path;
    $generic_img1 = '<img class="video-config-thumbnail" src="' . $base_path . gdpr_video_placeholder_img_path('generic_img1') . '"> ';
    $generic_img2 = '<img class="video-config-thumbnail" src="' . $base_path . gdpr_video_placeholder_img_path('generic_img2') . '"> ';
    $site_img_path = gdpr_video_placeholder_img_path('site_img');
    $site_img_filespec = \Drupal::root() . "/$site_img_path";
    if (file_exists($site_img_filespec)) {
      $site_img = '<img class="video-config-thumbnail" src="' . $base_path . $site_img_path . '"> ' . $this->t('site-specific placeholder image');
    }
    else {
      $site_img = $this->t('site-specific placeholder image, expected at <em>:filespec</em> -- please create an image of your liking, 640x360px preferred', [':filespec' => $site_img_filespec]);
    }
    $current_selection = $config->get('video_placeholder_img');
    $form['video_placeholder_img'] = [
      '#type' => 'radios',
      '#title' => $this->t('Placeholder image to display before video is started'),
      '#options' => [
        'generic_img1' => $generic_img1 . $this->t('generic placeholder image #1'),
        'generic_img2' => $generic_img2 . $this->t('generic placeholder image #2'),
        'site_img' => $site_img,
        'content_img' => $this->t('the first image on the page that contains the video to be played; if not present, auto-reverts to the site-specific placeholder above, or if that is not present, generic placeholder image #1'),
        'video_img' => $this->t('default frame taken from the video to be played'),
      ],
      '#default_value' => $current_selection ?? 'content_img',
      '#description' => $this->t("All options protect against premature downloading of cookies prior to consent. <br/>When using the bottom option it cannot be guaranteed that YouTube or Vimeo do not capture the visitor's IP address while the default frame is downloaded."),
    ];

    $form['texts'] = [
      '#type' => 'details',
      '#title' => $this->t('GDPR consent texts and confirmation button label'),
      '#description' => $description,
      '#open' => FALSE,
    ];
    $form['texts']['consent_text_youtube'] = [
      '#type' => 'textarea',
      '#title' => $this->t('GDPR consent text for YouTube, if used'),
      '#default_value' => $config->get('consent_text_youtube'),
    ];
    $form['texts']['consent_text_vimeo'] = [
      '#type' => 'textarea',
      '#title' => $this->t('GDPR consent text for Vimeo, if used'),
      '#default_value' => $config->get('consent_text_vimeo'),
    ];

    $form['texts']['consent_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consent confirmation button label'),
      '#default_value' => $config->get('consent_button_label'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('gdpr_video.settings');
    $keys = [
      'video_placeholder_img',
      'consent_text_youtube',
      'consent_text_vimeo',
      'consent_button_label',
    ];
    foreach ($keys as $key) {
      // These values are passed to JS via gdpr_video_page_attachments().
      $config->set($key, $form_state->getValue($key));
    }
    $config->save();

    parent::submitForm($form, $form_state);

    drupal_flush_all_caches();
  }

}
